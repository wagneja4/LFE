# LFE

Tool to extract PNG frames from Lottie animations using rlottie.

Codebase is a mess. You've been warned.

## Deps

- https://github.com/Samsung/rlottie -> Don't forget to do make install (you'll likely want to go with the cmake route)
- https://github.com/lvandeve/lodepng -> Already included here. Licensed Zlib.

You'll also need g++.

## How to build and install

```bash
make
```

To install:

```bash
sudo make install
```

## How to use

```bash
lfe mylottieanimation.json 420x420
```

This will extract all frames as PNG files in the working directory.

## Credit where it's due

- Most of the rlottie code is based on lottie2gif.cpp example from https://github.com/Samsung/rlottie (MIT)
- The PNG encoding related code is based on lodepng's examples
